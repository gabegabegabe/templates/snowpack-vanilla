module.exports = {
	extends: ['@gabegabegabe/eslint-config'],
	overrides: [
		{
			files: ['*.js'],
			extends: '@gabegabegabe/eslint-config/javascript'
		},
		{
			files: ['*.ts'],
			extends: '@gabegabegabe/eslint-config/typescript'
		},
		{
			files: ['*.test.ts'],
			extends: '@gabegabegabe/eslint-config/jest-typescript'
		}
	]
};
