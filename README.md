# Snowpack Vanilla
This template is for a website/app built using Snowpack, Pug, SASS, and
TypeScript, with no frameworks.

## Available Scripts
### npm start
Runs the app in the development mode. Open http://localhost:1234 to view it in
the browser.

The page will reload if you make edits. You will also see any lint errors in the
console.

### npm run build
Builds a static copy of your site to the `build/` folder. Your app is ready to
be deployed!

### npm run test
Executes all unit tests present in the project

### npm run lint
Lints all code in the project

#### npm run lint:markup
Lints only markup (HTML, Pug)

##### npm run lint:markup:html
Lints only HTML

##### npm run lint:markup:pug
Lints only Pug

#### npm run lint:scripts
Lints only scripts (TypeScript, JavaScript)

### npm run lint:styles
Lints only styles (SASS, CSS)
