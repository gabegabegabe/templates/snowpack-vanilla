/** @type {import("snowpack").SnowpackUserConfig } */
export default {
  mount: {
		public: { url: '/', static: true },
		src: { url: '/' },
  },
	exclude: [
		'**/node_modules/**/*',
		'**/src/testing/**/*',
		'**/src/**/*.test.ts'
	],
	plugins: [
		'@snowpack/plugin-typescript',
		'snowpack-plugin-pug',
		'@snowpack/plugin-sass',
		'@snowpack/plugin-postcss',
		'@snowpack/plugin-dotenv',
		['@snowpack/plugin-webpack', { sourceMap: true }]
	],
	devOptions: {
		port: 1234,
		open: 'none'
	},
	buildOptions: {
		out: 'dist'
	},
	alias: {
		'~': './src'
	}
};
